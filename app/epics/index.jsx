var {ajax} =require('rxjs/observable/dom/ajax');
var {getUser, userFetched}=require('./../actions/index');
import {Observable} from 'rxjs/Observable';
export const fetchUserEpic = action$ =>
    action$.ofType('GET_USER')
        .mergeMap(action =>
            ajax.getJSON(`https://api.github.com/users/${action.name}`)
                .map(response => userFetched(response))
                .catch(error => Observable.of({
                    type: 'FETCH_ERROR',
                    error: error.message,

                }))
        );
