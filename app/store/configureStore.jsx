var Redux = require('redux');
var {users}=require('./../reducers/index');
var {fetchUserEpic}=require('./../epics/index');
var Observable= require('redux-observable');


export var storeConfig = function () {
    var reducer = Redux.combineReducers({
        userReducer: users,
    });

    var epic = Observable.combineEpics(fetchUserEpic);

    var epicMiddleware = Observable.createEpicMiddleware(epic);
    var store = Redux.createStore(reducer, Redux.applyMiddleware(epicMiddleware));
    return store;
}