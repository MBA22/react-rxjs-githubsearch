var React = require('react');
var {connect} = require('react-redux');
var {bindActionCreators}=require('redux');


var actions = require('./../actions/index');
var Main = React.createClass({

    componentDidMount: function () {
        this.props.getProfile("mba");
    },
    searchProfile: function (e) {
        e.preventDefault();
        this.props.getProfile(e.target.value);
    },
    render: function () {
        console.log('looool', this.props.user);
        if (this.props.user.response) {
            return (
                <div className="container">
                    <div className="row">
                        <div className="col-xs-4 col-xs-offset-4">
                            <h1 className="text-center">Github Profile Search</h1>
                            <input className="form-control" onChange={this.searchProfile} type="text" placeholder="Enter Profile name here"/>
                        </div>
                        <img className="center-block" src={this.props.user.response.avatar_url} alt=""/>
                        <div className="well col-xs-4 col-xs-offset-4">
                            <h2 className="text-center">{this.props.user.response.name}</h2>
                            <h4 className="text-center">{this.props.user.response.email}</h4>
                            <h4 className="text-center">{this.props.user.response.location}</h4>
                        </div>
                    </div>
                </div>
            );
        }
        else
            return (
                <div className="container">
                    <h1 className="text-center">Github Profile Search</h1>
                    <input onChange={this.searchProfile} type="text"/>
                </div>
            );

    }
});

function mapStateToProps(state) {

    return {
        user: state.userReducer,
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        getProfile: actions.getUser,
    }, dispatch);
}

module.exports = connect(mapStateToProps, matchDispatchToProps)(Main);