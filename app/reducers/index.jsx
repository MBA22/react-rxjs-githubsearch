export var users = function (state = {}, action) {
    switch (action.type) {
        case 'GET_USER_FULFILLED':
            var newState = Object.assign({}, state, {response: action.response});
            return newState;
        case'FETCH_ERROR':
            var newState = Object.assign({}, state, {response:undefined,error: action.error});
            return newState;
        default:
            return state;
    }
};