export var getUser = function (userName) {
    return {
        type: 'GET_USER',
        name: userName,
    }
};
export var userFetched = function (response) {
    return {
        type: 'GET_USER_FULFILLED',
        response: response,
    }
};
