var React = require('react');
var ReactDOM = require('react-dom');
var Main = require('Main');
var {Provider} = require('react-redux');
var store = require('./store/configureStore').storeConfig();
var actions = require('./actions/index');
import 'rxjs';
var currentState = store.getState();

store.subscribe(function () {
    console.log('Current store state:', currentState);
});

ReactDOM.render(
    <Provider store={store}>
    <Main></Main>
    </Provider>
    , document.getElementById('app')
);
